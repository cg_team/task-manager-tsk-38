package ru.inshakov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.inshakov.tm.endpoint.*;
import ru.inshakov.tm.marker.SoapCategory;

import javax.xml.ws.WebServiceException;
import java.util.List;

public class AdminEndpointTest {

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @Nullable
    private Session session;

    @Before
    public void before() {
        session = sessionEndpoint.open("admin", "admin");
    }

    @Test
    @Category(SoapCategory.class)
    public void findAllByUserId() {
        sessionEndpoint.open("admin", "admin");
        List<Session> list = adminEndpoint.findAllByUserId(session, session.getUserId());
        Assert.assertTrue(list.size() > 1);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void closeAllByUserIdIncorrectSession() {
        sessionEndpoint.open("admin", "admin");
        adminEndpoint.closeAllByUserId(session, session.getUserId());
        List<Session> list = adminEndpoint.findAllByUserId(session, session.getUserId());
        Assert.assertTrue(list.size() > 1);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void closeAllByUserId() {
        sessionEndpoint.open("admin", "admin");
        adminEndpoint.closeAllByUserId(session, session.getUserId());
        List<Session> list = adminEndpoint.findAllByUserId(session, session.getUserId());
        Assert.assertEquals(1, list.size());
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void lockByLogin() {
        adminEndpoint.lockByLogin(session, "user");
        sessionEndpoint.open("user", "user");
    }

    @Test
    @Category(SoapCategory.class)
    public void unlockByLogin() {
        adminEndpoint.lockByLogin(session, "user");
        adminEndpoint.unlockByLogin(session, "user");
        sessionEndpoint.open("user", "user");
    }

}
