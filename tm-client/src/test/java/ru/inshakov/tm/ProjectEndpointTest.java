package ru.inshakov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.inshakov.tm.endpoint.*;
import ru.inshakov.tm.marker.SoapCategory;

import javax.xml.ws.WebServiceException;
import java.util.List;

public class ProjectEndpointTest {

    @NotNull
    private static final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private static final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @Nullable
    private static Session session;

    @Nullable
    private Project project;

    @BeforeClass
    public static void beforeClass() {
        session = sessionEndpoint.open("user", "user");
    }

    @Before
    public void before() {
        project = new Project();
        project.setName("Test");
        project.setId("1");
        projectEndpoint.addProject(session, project);
    }

    @After
    public void after() {
        projectEndpoint.removeProjectById(session, project.getId());
    }

    @AfterClass
    public static void afterClass() {
        sessionEndpoint.close(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("Test", project.getName());

        @NotNull final Project projectById = projectEndpoint.findProjectById(session, project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project.getId(), projectById.getId());
    }

    @Test
    @Category(SoapCategory.class)
    public void findAll() {
        @NotNull final List<Project> projects = projectEndpoint.findProjectAll(session);
        Assert.assertTrue(projects.size() > 0);
    }


    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findAllByUserIdIncorrect() {
        projectEndpoint.findProjectAll(new Session());
    }

    @Test
    @Category(SoapCategory.class)
    public void findById() {
        @NotNull final Project project = projectEndpoint.findProjectById(session, this.project.getId());
        Assert.assertNotNull(project);
    }

    @Test
    @Category(SoapCategory.class)
    public void findByIdIncorrect() {
        @NotNull final Project project = projectEndpoint.findProjectById(session, "34");
        Assert.assertNull(project);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIdNull() {
        projectEndpoint.findProjectById(session, null);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIdIncorrectUser() {
        projectEndpoint.findProjectById(new Session(), this.project.getId());
    }

    @Test
    @Category(SoapCategory.class)
    public void findByName() {
        @NotNull final Project project = projectEndpoint.findProjectByName(session, "Test");
        Assert.assertNotNull(project);
    }

    @Test
    @Category(SoapCategory.class)
    public void findByNameIncorrect() {
        @NotNull final Project project = projectEndpoint.findProjectByName(session, "34");
        Assert.assertNull(project);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByNameNull() {
        projectEndpoint.findProjectByName(session, null);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByNameIncorrectUser() {
        projectEndpoint.findProjectByName(new Session(), this.project.getName());
    }

    @Test
    @Category(SoapCategory.class)
    public void findByIndex() {
        @NotNull final Project project = projectEndpoint.findProjectByIndex(session, 0);
        Assert.assertNotNull(project);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIndexIncorrect() {
        projectEndpoint.findProjectByIndex(session, 34);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIndexNull() {
        projectEndpoint.findProjectByIndex(session, null);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIndexIncorrectUser() {
        projectEndpoint.findProjectByIndex(new Session(), 0);
    }

    @Test
    @Category(SoapCategory.class)
    public void removeById() {
        projectEndpoint.removeProjectById(session, project.getId());
        Assert.assertNull(projectEndpoint.findProjectById(session, project.getId()));
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIdNull() {
        projectEndpoint.removeProjectById(session, null);
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByIdIncorrect() {
        projectEndpoint.removeProjectById(session, "34");
        Assert.assertNull(project);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIdIncorrectUser() {
        projectEndpoint.removeProjectById(new Session(), this.project.getId());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByIndex() {
        projectEndpoint.removeProjectByIndex(session, 0);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIndexIncorrect() {
        projectEndpoint.removeProjectByIndex(session, 34);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIndexNull() {
        projectEndpoint.removeProjectByIndex(session, null);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIndexIncorrectUser() {
        projectEndpoint.removeProjectByIndex(new Session(), 0);
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByName() {
        projectEndpoint.removeProjectByName(session, "Test");
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByNameIncorrect() {
        projectEndpoint.removeProjectByName(session, "34");
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByNameNull() {
        projectEndpoint.removeProjectByName(session, null);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByNameIncorrectUser() {
        projectEndpoint.removeProjectByName(new Session(), this.project.getName());
    }

}
