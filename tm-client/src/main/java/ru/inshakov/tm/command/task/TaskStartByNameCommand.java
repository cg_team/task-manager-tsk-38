package ru.inshakov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.TaskAbstractCommand;
import ru.inshakov.tm.endpoint.Task;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.util.TerminalUtil;

public class TaskStartByNameCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-start-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start task by name.";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final Task task = serviceLocator.getTaskEndpoint().startTaskByName(getSession(), name);
        if (task == null) throw new TaskNotFoundException();
    }
}
