package ru.inshakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.inshakov.tm.marker.DBCategory;
import ru.inshakov.tm.model.User;
import ru.inshakov.tm.service.ConnectionService;
import ru.inshakov.tm.service.PropertyService;

import java.util.List;

public class UserRepositoryTest {

    @Nullable
    private UserRepository userRepository;

    @Nullable
    private User user;

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        userRepository = new UserRepository(connectionService.getConnection());
        @NotNull final User user = new User();
        user.setLogin("User");
        user.setEmail("email");
        user.setPasswordHash("1");
        this.user = userRepository.add(user);
    }

    @Test
    @Category(DBCategory.class)
    public void add() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("User", user.getLogin());
        Assert.assertNotNull(user.getEmail());
        Assert.assertEquals("email", user.getEmail());

        @NotNull final User userById = userRepository.findById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(user.getId(), userById.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        @NotNull final List<User> users = userRepository.findAll();
        Assert.assertTrue(users.size() > 0);
    }

    @Test
    @Category(DBCategory.class)
    public void findById() {
        @NotNull final User user = userRepository.findById(this.user.getId());
        Assert.assertNotNull(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrect() {
        @NotNull final User user = userRepository.findById("34");
        Assert.assertNull(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdNull() {
        @NotNull final User user = userRepository.findById(null);
        Assert.assertNull(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findByLogin() {
        @NotNull final User user = userRepository.findByLogin(this.user.getLogin());
        Assert.assertNotNull(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findByLoginIncorrect() {
        @NotNull final User user = userRepository.findByLogin("34");
        Assert.assertNull(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findByLoginNull() {
        @NotNull final User user = userRepository.findByLogin(null);
        Assert.assertNull(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findByEmail() {
        @NotNull final User user = userRepository.findByEmail(this.user.getEmail());
        Assert.assertNotNull(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findByEmailIncorrect() {
        @NotNull final User user = userRepository.findByEmail("34");
        Assert.assertNull(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findByEmailNull() {
        @NotNull final User user = userRepository.findByEmail(null);
        Assert.assertNull(user);
    }

    @Test
    @Category(DBCategory.class)
    public void remove() {
        userRepository.removeById(user.getId());
        Assert.assertNull(userRepository.findById(user.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void removeById() {
        userRepository.removeById(user.getId());
        Assert.assertNull(userRepository.findById(user.getId()));
    }

}