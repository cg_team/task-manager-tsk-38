package ru.inshakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.inshakov.tm.marker.DBCategory;
import ru.inshakov.tm.model.Session;
import ru.inshakov.tm.service.ConnectionService;
import ru.inshakov.tm.service.PropertyService;

import java.util.List;

public class SessionRepositoryTest {

    @Nullable
    private SessionRepository sessionRepository;

    @Nullable
    private Session session;

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        sessionRepository = new SessionRepository(connectionService.getConnection());
        @NotNull final Session session = new Session();
        session.setUserId("userId");
        session.setTimestamp(1L);
        session.setSignature("1");
        this.session = sessionRepository.add(session);
    }

    @Test
    @Category(DBCategory.class)
    public void add() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getUserId());
        Assert.assertEquals("userId", session.getUserId());

        @NotNull final Session sessionById = sessionRepository.findById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(session.getId(), sessionById.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void findById() {
        @NotNull final Session session = sessionRepository.findById(this.session.getId());
        Assert.assertNotNull(session);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrect() {
        @NotNull final Session session = sessionRepository.findById("34");
        Assert.assertNull(session);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdNull() {
        @NotNull final Session session = sessionRepository.findById(null);
        Assert.assertNull(session);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserId() {
        @NotNull final List<Session> session = sessionRepository.findAllByUserId(this.session.getUserId());
        Assert.assertNotNull(session);
        Assert.assertEquals(1, session.size());
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Session> session = sessionRepository.findAllByUserId("34");
        Assert.assertNotNull(session);
        Assert.assertNotEquals(1, session.size());
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserIdNull() {
        @NotNull final List<Session> session = sessionRepository.findAllByUserId(null);
        Assert.assertEquals(0, session.size());
    }

    @Test
    @Category(DBCategory.class)
    public void removeByUserId() {
        sessionRepository.removeByUserId(this.session.getUserId());
        Assert.assertEquals(0, sessionRepository.findAllByUserId(this.session.getUserId()).size());
    }

    @Test
    @Category(DBCategory.class)
    public void removeByUserIdIncorrect() {
        sessionRepository.removeByUserId("34");
        Assert.assertEquals(1, sessionRepository.findAllByUserId(this.session.getUserId()).size());
    }

    @Test
    @Category(DBCategory.class)
    public void removeByUserIdNull() {
        sessionRepository.removeByUserId(null);
        Assert.assertEquals(1, sessionRepository.findAllByUserId(this.session.getUserId()).size());
    }

    @Test
    @Category(DBCategory.class)
    public void remove() {
        sessionRepository.removeById(session.getId());
        Assert.assertNull(sessionRepository.findById(session.getId()));
    }

}