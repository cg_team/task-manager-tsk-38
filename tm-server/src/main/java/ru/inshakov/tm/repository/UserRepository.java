package ru.inshakov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.IUserRepository;
import ru.inshakov.tm.constant.FieldConst;
import ru.inshakov.tm.constant.TableConst;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    protected String getTableName() {
        return TableConst.USER_TABLE;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(final String login) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE login = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final User result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(final String email) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE email = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, email);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final User result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public void removeUserByLogin(final String login) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE login = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    protected User fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setEmail(row.getString(FieldConst.EMAIL));
        user.setLogin(row.getString(FieldConst.LOGIN));
        user.setId(row.getString(FieldConst.ID));
        user.setRole(Role.valueOf(row.getString(FieldConst.ROLE)));
        user.setLocked(row.getBoolean(FieldConst.LOCKED));
        user.setFirstName(row.getString(FieldConst.FIRST_NAME));
        user.setLastName(row.getString(FieldConst.LAST_NAME));
        user.setMiddleName(row.getString(FieldConst.MIDDLE_NAME));
        user.setPasswordHash(row.getString(FieldConst.PASSWORD_HASH));
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User add(@Nullable final User entity) {
        if (entity == null) return null;
        @NotNull final String query = "INSERT INTO " + getTableName() +
                "(id, email, login, role, locked, first_name, last_name, middle_name, password_hash)" +
                "VALUES(?,?,?,?,?,?,?,?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getEmail());
        statement.setString(3, entity.getLogin());
        statement.setString(4, entity.getRole().toString());
        statement.setBoolean(5, entity.isLocked());
        statement.setString(6, entity.getFirstName());
        statement.setString(7, entity.getLastName());
        statement.setString(8, entity.getMiddleName());
        statement.setString(9, entity.getPasswordHash());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User update(@Nullable final User entity) {
        if (entity == null) return null;
        @NotNull final String query = "UPDATE " + getTableName() +
                "SET email=?, login=?, role=?, locked=?, first_name=?, last_name=?, middle_name=? WHERE id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getEmail());
        statement.setString(2, entity.getLogin());
        statement.setString(3, entity.getRole().toString());
        statement.setBoolean(4, entity.isLocked());
        statement.setString(5, entity.getFirstName());
        statement.setString(6, entity.getLastName());
        statement.setString(7, entity.getLastName());
        statement.setString(8, entity.getId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }
}
