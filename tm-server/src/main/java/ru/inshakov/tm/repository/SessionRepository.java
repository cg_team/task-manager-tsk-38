package ru.inshakov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.ISessionRepository;
import ru.inshakov.tm.constant.FieldConst;
import ru.inshakov.tm.constant.TableConst;
import ru.inshakov.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    protected String getTableName() {
        return TableConst.SESSION_TABLE;
    }

    @Override
    @Nullable
    @SneakyThrows
    public List<Session> findAllByUserId(@Nullable final String userId) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Session> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public void removeByUserId(@Nullable final String userId) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    protected Session fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setUserId(row.getString(FieldConst.USER_ID));
        session.setTimestamp(row.getLong(FieldConst.TIMESTAMP));
        session.setId(row.getString(FieldConst.ID));
        session.setSignature(row.getString(FieldConst.SIGNATURE));
        return session;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session add(@Nullable final Session entity) {
        if (entity == null) return null;
        @NotNull final String query = "INSERT INTO " + getTableName() +
                "(id, \"timestamp\", signature, user_id)" +
                "VALUES(?,?,?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setLong(2, entity.getTimestamp());
        statement.setString(3, entity.getSignature());
        statement.setString(4, entity.getUserId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session update(@Nullable final Session entity) {
        if (entity == null) return null;
        @NotNull final String query = "UPDATE " + getTableName() +
                "SET id=?, \"timestamp\"=?, signature=?, user_id=? WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setLong(1, entity.getTimestamp());
        statement.setString(2, entity.getSignature());
        statement.setString(3, entity.getUserId());
        statement.setString(4, entity.getId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }
}
