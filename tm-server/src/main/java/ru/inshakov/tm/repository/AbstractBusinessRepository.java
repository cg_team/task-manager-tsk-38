package ru.inshakov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.model.AbstractBusinessEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> {

    public AbstractBusinessRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Nullable
    @SneakyThrows
    public E findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE id = ? AND user_id= ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    public void addAll(@NotNull final String userId, @Nullable final Collection<E> collection) {
        if (collection == null) return;
        for (E item : collection) {
            item.setUserId(userId);
            add(item);
        }
    }

    @Nullable
    @SneakyThrows
    public E add(@NotNull final String userId, @Nullable final E entity) {
        if (entity == null) return null;
        entity.setUserId(userId);
        return add(entity);
    }

    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ? AND user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ? AND user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

}
