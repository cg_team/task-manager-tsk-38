package ru.inshakov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Setter
@Getter
public abstract class AbstractBusinessEntity extends AbstractEntity {

    @Nullable
    protected String userId;

}
