package ru.inshakov.tm.exception.system;

public final class ConnectionFailedException extends RuntimeException {

    public ConnectionFailedException() {
        super("Error! Connection failed...");
    }

}