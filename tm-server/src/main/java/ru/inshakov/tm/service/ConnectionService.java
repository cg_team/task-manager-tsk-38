package ru.inshakov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.exception.system.ConnectionFailedException;

import java.sql.Connection;
import java.sql.DriverManager;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @Override
    @SneakyThrows
    public Connection getConnection() {
        @Nullable final String username = propertyService.getJdbcUser();
        if (username == null) throw new ConnectionFailedException();
        @Nullable final String password = propertyService.getJdbcPassword();
        if (password == null) throw new ConnectionFailedException();
        @Nullable final String url = propertyService.getJdbcUrl();
        if (url == null) throw new ConnectionFailedException();
        @NotNull final Connection connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(false);
        return connection;
    }

}
