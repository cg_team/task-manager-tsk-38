package ru.inshakov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.IProjectRepository;
import ru.inshakov.tm.api.repository.ITaskRepository;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.api.service.IProjectTaskService;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyIndexException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.model.Task;
import ru.inshakov.tm.repository.ProjectRepository;
import ru.inshakov.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.findAllTaskByProjectId(userId, projectId);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task bindTaskById(
            @NotNull final String userId, @Nullable final String taskId, @Nullable final String projectId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            @Nullable final Task task = taskRepository.bindTaskToProjectById(userId, taskId, projectId);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task unbindTaskById(@NotNull final String userId, @Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            @Nullable final Task task = taskRepository.unbindTaskById(userId, taskId);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }


    @Override
    @SneakyThrows
    public void removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final String projectId = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new)
                    .getId();
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final String projectId = Optional.ofNullable(projectRepository.findByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new)
                    .getId();
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }
}

