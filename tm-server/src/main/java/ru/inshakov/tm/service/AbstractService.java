package ru.inshakov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IRepository;
import ru.inshakov.tm.api.IService;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.model.AbstractEntity;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    public abstract IRepository<E> getRepository(@NotNull Connection connection);

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll() {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findAll();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<E> collection) {
        @NotNull final Optional<Collection<E>> optional = Optional.ofNullable(collection);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            optional.ifPresent(repository::addAll);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E add(@Nullable final E entity) {
        if (entity == null) return null;
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            @Nullable final E entityResult = repository.add(entity);
            connection.commit();
            return entityResult;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.removeById(optionalId.orElseThrow(EmptyIdException::new));
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final E entity) {
        if (entity == null) return;
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.remove(entity);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }
}
