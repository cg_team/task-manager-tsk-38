package ru.inshakov.tm.constant;

public interface TableConst {

    String TABLE_PREFIX = "tm_";

    String SESSION_TABLE = TABLE_PREFIX + "session";

    String USER_TABLE = TABLE_PREFIX + "user";

    String TASK_TABLE = TABLE_PREFIX + "task";

    String PROJECT_TABLE = TABLE_PREFIX + "project";
}
