package ru.inshakov.tm.api;

import ru.inshakov.tm.model.AbstractBusinessEntity;

import java.util.Collection;
import java.util.List;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IService<E> {

    List<E> findAll(final String userId);

    void addAll(final String userId, final Collection<E> collection);

    E add(final String userId, final E entity);

    E findById(final String userId, final String id);

    void clear(final String userId);

    void removeById(final String userId, final String id);

    void remove(final String userId, final E entity);

}

