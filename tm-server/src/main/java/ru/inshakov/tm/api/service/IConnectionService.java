package ru.inshakov.tm.api.service;

import lombok.SneakyThrows;

import java.sql.Connection;

public interface IConnectionService {
    @SneakyThrows
    Connection getConnection();
}
