package ru.inshakov.tm.api.service;

import ru.inshakov.tm.api.IBusinessService;
import ru.inshakov.tm.model.Task;

public interface ITaskService extends IBusinessService<Task> {

    Task findByName(String userId, String name);

    Task findByIndex(String userId, Integer index);

    void removeByName(String userId, String name);

    void removeByIndex(String userId, Integer index);

    Task updateById(String userId, final String id, final String name, final String description);

    Task updateByIndex(String userId, final Integer index, final String name, final String description);

    Task startById(String userId, String id);

    Task startByIndex(String userId, Integer index);

    Task startByName(String userId, String name);

    Task finishById(String userId, String id);

    Task finishByIndex(String userId, Integer index);

    Task finishByName(String userId, String name);

    Task add(String userId, String name, String description);
}
