package ru.inshakov.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IRepository;
import ru.inshakov.tm.model.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @Nullable List<Session> findAllByUserId(@Nullable String userId);

    void removeByUserId(@Nullable String userId);

    @Nullable
    @SneakyThrows
    Session add(@Nullable Session entity);

    @Nullable
    @SneakyThrows
    Session update(@Nullable Session entity);

}
