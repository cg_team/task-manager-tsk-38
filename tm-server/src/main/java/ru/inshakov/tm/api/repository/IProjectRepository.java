package ru.inshakov.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IBusinessRepository;
import ru.inshakov.tm.model.Project;

public interface IProjectRepository extends IBusinessRepository<Project> {

    Project findByName(final String userId, final String name);

    Project findByIndex(final String userId, final int index);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final int index);

    @Nullable Project add(@NotNull String userId, @Nullable String name, @Nullable String description);

    @Nullable
    @SneakyThrows
    Project add(@Nullable Project entity);

    @Nullable
    @SneakyThrows
    Project update(@Nullable Project entity);

}
