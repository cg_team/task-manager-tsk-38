package ru.inshakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IBusinessRepository;
import ru.inshakov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    List<Task> findAllTaskByProjectId(final String userId, final String projectId);

    void removeAllTaskByProjectId(final String userId, final String projectId);

    Task bindTaskToProjectById(final String userId, final String taskId, final String projectId);

    Task unbindTaskById(final String userId, final String id);

    Task findByName(final String userId, final String name);

    Task findByIndex(final String userId, final int index);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final int index);

    @Nullable Task add(@NotNull String userId, @Nullable String name, @Nullable String description);

    Task add(@Nullable Task entity);

    Task update(@Nullable Task entity);

}
